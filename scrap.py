from bs4 import BeautifulSoup
import urllib.request as req
import urllib
import os
import time
from urllib.parse import urljoin
from urllib.parse import urlparse
from urllib.parse import urlsplit
import requests
import pandas as pd



lists = [
    'https://www.eiken.or.jp/eiken/exam/grade_1/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_p1/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_2/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_p2/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_3/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_4/solutions.html'
    ,'https://www.eiken.or.jp/eiken/exam/grade_5/solutions.html'
]

base_url = 'https://www.eiken.or.jp'

for site in lists :
    html_text = requests.get( site ).content
    soup = BeautifulSoup(html_text, 'html.parser', from_encoding='utf-8')
    link = soup.body.find_all( "a" )
    url = urlparse( site )

    grade = url.path.split('/')[3]

    print( grade )

    dir_path = './data/' + grade

    os.makedirs(dir_path, exist_ok=True)

    for href in link :
        link_href = href.get('href')
        link_text = href.get_text()

        # 相対パスを絶対パスに変更
        link_href = urllib.parse.urljoin(base_url, link_href)

        if '.pdf' in link_href or '.mp3' in link_href :
            if '問題冊子' in link_text or 'リスニング原稿' in link_text or 'リスニング音源' in link_text or '解答' in link_text :

                # Only .pdf .mp3
                # if '.pdf' in link_href or '.mp3' in link_href :

                data = urllib.request.urlopen(link_href).read()

                if '.mp3' in link_href or '解答' in link_text :
                    with open(dir_path + '/' + link_href.split('/')[-2] + '-' + link_href.split('/')[-1] , mode="wb") as f:
                        f.write(data)
                        print( str(link_href) + '保存しました。' )
                else:
                    with open(dir_path + '/' + link_href.split('/')[-1] , mode="wb") as f:
                        f.write(data)
                        print( str(link_href) + '保存しました。' )

    # time.sleep(30)